package br.com.duxusdesafio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.duxusdesafio.model.ComposicaoTime;
import br.com.duxusdesafio.repository.ComposicaoTimeRepository;

@RestController
@CrossOrigin(origins ="*", allowedHeaders = "*")
@RequestMapping("/comp")
public class ComposicaoTimeController {
 @Autowired
 	private ComposicaoTimeRepository repository;
 
 @GetMapping
 public ResponseEntity<List<ComposicaoTime>> getAll(){
	 return ResponseEntity.ok(repository.findAll());
 }
 
 
}
